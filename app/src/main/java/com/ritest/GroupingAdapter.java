package com.ritest;

import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListAdapter;
import android.widget.SectionIndexer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by koma on 04/07/14.
 */
public class GroupingAdapter<Header> extends BaseAdapter implements SectionIndexer, Filterable {

        public interface GroupSplitter<Header> {
            Header createHeader();
            void extractHeaderFromItem(Object item, Header header);
            boolean areHeadersEqual(Header a, Header b);
        }

        public GroupingAdapter(
                ListAdapter adapter,
                ArrayAdapter<Header> headers_adapter,
                GroupSplitter<Header> grouping
        ) {
            this.adapter = adapter;
            this.grouping = grouping;
            this.headers_adapter = headers_adapter;

            groupEnds = new ArrayList<Integer>();

            adapter.registerDataSetObserver(new DataSetObserver() {
                @Override
                public void onChanged() {
                    calculateGroups();
                    notifyDataSetChanged();
                }

                @Override
                public void onInvalidated() {
                    groupEnds.clear();
                    notifyDataSetInvalidated();
                }
            });

            calculateGroups();
        }

        public int getCount() {
            return groupEnds.size() + adapter.getCount();
        }

        public Object getItem(int position) {
            int group_index = getItemGroupIndex(position);
            int group_header_position = getGroupHeaderPosition(group_index);

            if (position == group_header_position) {
                return null;
            } else {
                // Subtract (group_index+1) headers before element.
                int item_pos = getAdapterItemPosition(position, group_index);
                return adapter.getItem(item_pos);
            }
        }

        public long getItemId(int position) {
            int group_index = getItemGroupIndex(position);
            int group_header_position = getGroupHeaderPosition(group_index);
            if (position == group_header_position) {
                return -1;
            } else {
                int adapter_item_position = getAdapterItemPosition(position, group_index);
                return adapter.getItemId(adapter_item_position);
            }
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            int group_index = getItemGroupIndex(position);
            int group_header_position = getGroupHeaderPosition(group_index);
            if (position == group_header_position) {
                return headers_adapter.getView(group_index, convertView, parent);
            } else {
                int adapter_item_position = getAdapterItemPosition(position, group_index);
                return adapter.getView(adapter_item_position, convertView, parent);
            }
        }

        public boolean isEnabled(int position) {
            int group_index = getItemGroupIndex(position);
            int group_header_position = getGroupHeaderPosition(group_index);
            if (position == group_header_position) {
                return headers_adapter.isEnabled(group_index);
            } else {
                int adapter_item_position = getAdapterItemPosition(position, group_index);
                return adapter.isEnabled(adapter_item_position);
            }
        }

        public int getViewTypeCount() {
            return headers_adapter.getViewTypeCount() + adapter.getViewTypeCount();
        }

        public int getItemViewType(int position) {
            int headers_view_count = headers_adapter.getViewTypeCount();
            int group_index = getItemGroupIndex(position);
            int group_header_position = getGroupHeaderPosition(group_index);
            if (position == group_header_position) {
                return headers_adapter.getItemViewType(group_index);
            } else {
                int adapter_item_position = getAdapterItemPosition(position, group_index);
                return adapter.getItemViewType(adapter_item_position) + headers_view_count;
            }
        }

        public boolean areAllItemsEnabled() {
            return adapter.areAllItemsEnabled() && headers_adapter.areAllItemsEnabled();
        }

        public void registerDataSetObserver(DataSetObserver observer) {
            adapter.registerDataSetObserver(observer);
        }

        public void unregisterDataSetObserver(DataSetObserver observer) {
            adapter.unregisterDataSetObserver(observer);
        }

        public boolean hasStableIds() {
            return false;
        }

        public boolean isEmpty() {
            return groupEnds.isEmpty();
        }

        public int getPositionForSection(int section) {
            return section == 0 ? 0 : groupEnds.get(section-1)+1;
        }

        public int getSectionForPosition(int position) {
            return getItemGroupIndex(position);
        }

        public Object[] getSections() {
            Object[] sections = new Object[headers_adapter.getCount()];
            for (int i = 0; i < sections.length; ++i)
                sections[i] = headers_adapter.getItem(i);
            return sections;
        }

        public Filter getFilter() {
            if (adapter instanceof Filterable)
                return ((Filterable)adapter).getFilter();
            else
                throw new UnsupportedOperationException("Underlying adapter doesn't implement Filterable.");
        }

        public boolean isGroupHeader(int position) {
            int group_index = getItemGroupIndex(position);
            int group_header_position = getGroupHeaderPosition(group_index);
            return position == group_header_position;
        }

        /**
         * Get position in underlying adapter corresponding to
         * given position.
         * @param position Position in GroupingAdapter.
         * @return Either item position in underlying adapter or -1 if source
         * position corresponds to group header.
         */
        public int positionToAdapterPosition(int position) {
            int group_index = getItemGroupIndex(position);
            int group_header_position = getGroupHeaderPosition(group_index);

            if (position == group_header_position) {
                return -1;
            } else {
                return getAdapterItemPosition(position, group_index);
            }
        }

        /**
         * Get position corresponding to given position in underlying
         * adapter.
         * @param position Position in underlying adapter.
         * @return Position in GroupingAdapter.
         * @throws IllegalArgumentException if position is out of bounds.
         */
        public int adapterPositionToPosition(int position) {
            // Not even nearly optimal, I know.
            int groups_count = groupEnds.size();
            for (int i = 0; i < groups_count; ++i) {
                int underlying_group_end = groupEnds.get(i) - (i+1);
                if (position <= underlying_group_end)
                    return position + (i+1);
            }

            throw new IllegalArgumentException(String.format(
                    "Position %d is not valid for adapter with %d items.",
                    position, adapter.getCount()
            ));
        }

        protected int getAdapterItemPosition(int position, int group_index) {
            return position - (group_index+1);
        }

        protected void calculateGroups() {
            groupEnds.clear();
            headers_adapter.clear();

            int itemsCount = adapter.getCount();
            if (itemsCount > 0) {
                Header previousHeader = grouping.createHeader();
                Header header = grouping.createHeader();

                Object previousItem = adapter.getItem(0);
                grouping.extractHeaderFromItem(previousItem, previousHeader);

                for (int i = 1; i < itemsCount; ++i) {
                    Object item = adapter.getItem(i);
                    grouping.extractHeaderFromItem(item, header);

                    if (! grouping.areHeadersEqual(previousHeader, header)) {
                        // Found first item of next group. So last
                        // item of current group has position (i-1).
                        // But there are groupEnds.size() headers
                        // of previous groups + one header for current
                        // group. So total group end position is
                        // (i-1) + (groupEnds.size()+1) = i + groupEnds.size()
                        groupEnds.add(i + groupEnds.size());
                        headers_adapter.add(previousHeader);

                        previousHeader = header;
                        header = grouping.createHeader();
                    }

                    previousItem = item;
                }

                groupEnds.add(itemsCount + groupEnds.size());
                headers_adapter.add(previousHeader);
            }
        }

        protected int getItemGroupIndex(int position) throws AssertionError {
            int group_index = Collections.binarySearch(groupEnds, position);
            if (group_index < 0) {
                // No exact match.
                // Get insertion point - position of first element which
                // is greater than requested or size of list of all elements
                // a less.
                group_index = -(group_index+1);
                if (group_index == groupEnds.size())
                    throw new AssertionError("Invalid element position requested");
            }
            return group_index;
        }

        /**
         * Get position of group header.
         */
        protected int getGroupHeaderPosition(int group_index) {
            int group_header_position =	group_index == 0
                    ? 0
                    : groupEnds.get(group_index-1)+1;
            return group_header_position;
        }

        /** Underlying items adapter. */
        private final ListAdapter adapter;

        private final ArrayAdapter<Header> headers_adapter;

        private final GroupSplitter<Header> grouping;

        /**
         * Positions of group's endings in resulting adapter.
         * i'th element of this array stores position (in resulting adapter) of last element
         * in i'th group.
         *
         * Say, we have items (0, 0, 1, 1, 1, 2, 3, 3, 3, 3)
         * Items grouped: (0, 0), (1, 1, 1), (2), (3, 3, 3, 3)
         * Items with group headers (marked as *): (*, 0, 0), (*, 1, 1, 1), (*, 2), (*, 3, 3, 3, 3)
         * Positions:                               0  1  2    3  4  5  6    7  8    9,10,11,12,13
         * groupEnds: (2, 6, 8, 13).
         */
        private final List<Integer> groupEnds;
}
