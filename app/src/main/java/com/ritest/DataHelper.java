package com.ritest;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by koma on 04/07/14.
 */
public class DataHelper  extends SQLiteOpenHelper {

    //database version, current ver is 1.
    public static final int DATABASE_VER=1;

    //database Name or db name
    public static final String DATABASE_NAME="products.db";

    //table Name, table person
    public static final String TABLE_PRODUCT ="product";

    //table fields name,fist name,email and domain
    public static final String KEY_NAME="name";
    public static final String KEY_PRICE ="price";
    public static final String KEY_BRAND ="brand";


    public DataHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VER);
    }



    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        //creating string sqlTable for creating a table
        String sqlTable = "create table " + TABLE_PRODUCT + "(" +KEY_NAME+ " text,"+ KEY_PRICE +
                " text," + KEY_BRAND + " text);";
        //db.execSQL() will execute string which we provide and will create a table with given table name and fields.
        db.execSQL(sqlTable);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub

    }

}
