package com.ritest;

import android.app.Activity;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.common.collect.ComparisonChain;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by koma on 04/07/14.
 */
public class MainActivity extends Activity {

    //EditText etResponse;
    //TextView tvIsConnected;

    private ArrayList<Product> mProductsList;

    private ListView mProductListView;

    private ProductAdapter mAdpt;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        // get reference to the views
        //etResponse = (EditText) findViewById(R.id.etResponse);
        //tvIsConnected = (TextView) findViewById(R.id.tvIsConnected);

        mProductListView = (ListView) findViewById(R.id.productList);

        // check if you are connected or not
        if(isConnected()){

            mProductsList = new ArrayList<Product>();

            // call AsynTask to perform network operation on separate thread
            new HttpAsyncTask().execute("https://www.zalora.com.my/mobile-api/women/clothing?maxitems=10");
        }
        else{
            Toast.makeText(getBaseContext(), "Not connected!", Toast.LENGTH_LONG).show();
        }


    }

    public static String GET(String url){
        InputStream inputStream = null;
        String result = "";
        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();

            // convert inputstream to string
            if(inputStream != null)
                result = convertInputStreamToString(inputStream);
            else
                result = "Did not work!";

        } catch (Exception e) {
            Log.d("InputStream", e.getLocalizedMessage());
        }

        return result;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    public boolean isConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            return GET(urls[0]);
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {

            Toast.makeText(getBaseContext(), "Received!", Toast.LENGTH_LONG).show();

            parseJSON(result);

        }

    }

    private void parseJSON(String a_json){

        try {


            JSONObject json = new JSONObject(a_json);

            JSONArray productsArray = json.getJSONObject("metadata").getJSONArray("results");

            Product p;

            JSONObject singleProduct;

            JSONObject singleProductDataObject;

            for(int i = 0; i < productsArray.length(); i++){

                p = new Product();

                singleProduct = productsArray.getJSONObject(i);

                singleProductDataObject = productsArray.getJSONObject(i).getJSONObject("data");

                p.setProductImageUrl(singleProduct.getJSONArray("images").getJSONObject(0).getString("path"));

                p.setProductBrand(singleProductDataObject.getString("brand"));

                p.setProductName(singleProductDataObject.getString("name"));

                p.setProductPrice(singleProductDataObject.getString("price"));

                mProductsList.add(p);

            }

            addSortingHeader();
            populateProductsListView();

        }catch(Exception e){

            Log.e("RiTes", "Error parsing json");

        }

    }

    private ViewGroup header;

    private void addSortingHeader(){

        LayoutInflater inflater = getLayoutInflater();

        header = (ViewGroup)inflater.inflate(R.layout.list_header, mProductListView, false);

        Button sortByNameBtn = (Button)  header.findViewById(R.id.sortNameBtn);
        final Button sortByPriceBtn = (Button) header.findViewById(R.id.sortPriceBtn);
        Button sortByBrandBtn = (Button) header.findViewById(R.id.sortBrandBtn);

        sortByNameBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sortListByName();
                refreshProductsListView();
            }
        });

        sortByPriceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sortListByPrice();
                refreshProductsListView();

            }
        });

        sortByBrandBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sortListByBrand();
                refreshProductsListView();

            }
        });

    }

    private void populateProductsListView(){

        mAdpt = new ProductAdapter(mProductsList, getBaseContext());

        mProductListView.addHeaderView(header, null, false);

        mProductListView.setAdapter(mAdpt);
    }

    private void refreshProductsListView(){

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAdpt.notifyDataSetChanged();

            }
        });

    }

    private void sortListByName(){

        Collections.sort(mProductsList, new Comparator<Product>() {
            @Override
            public int compare(Product p1, Product p2) {
                return ComparisonChain.start().compare(p1.getProductName(), p2.getProductName()).result();

            }
        });


    }

    private void sortListByPrice(){

        Collections.sort(mProductsList, new Comparator<Product>() {
            @Override
            public int compare(Product p1, Product p2) {
                return ComparisonChain.start().compare(p1.getProductPrice(), p2.getProductPrice()).result();

            }
        });

    }

    private void sortListByBrand(){

        Collections.sort(mProductsList, new Comparator<Product>() {
            @Override
            public int compare(Product p1, Product p2) {
                return ComparisonChain.start().compare(p1.getProductBrand(), p2.getProductBrand()).result();

            }
        });

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.saveDB:
                saveProductDataToDB();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private DataHelper mDbHelper;

    private SQLiteDatabase mDatabase;

    private void saveProductDataToDB(){

        mDbHelper = new DataHelper(this);
        // getting database for reading/writing purpose
        mDatabase = mDbHelper.getWritableDatabase();

        for(Product p : mProductsList) {

            // getting data from edit text to string variables
            String name = p.getProductName();
            String brand = p.getProductBrand();
            String price = p.getProductPrice();



                // contentValues will add data into key value pair which
                // will later store in db
                ContentValues values = new ContentValues();

                values.put(DataHelper.KEY_NAME, name);
                values.put(DataHelper.KEY_BRAND, brand);
                values.put(DataHelper.KEY_PRICE, price);

                // insert query will insert data in database with
                // contentValues pair.
                mDatabase.insert(DataHelper.TABLE_PRODUCT, null, values);


        }

        // closing the database connection
        mDatabase.close();

        Toast.makeText(getBaseContext(), "Database populated", Toast.LENGTH_LONG).show();


    }
}
