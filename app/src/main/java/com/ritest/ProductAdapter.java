package com.ritest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class ProductAdapter extends ArrayAdapter<Product> {

    private List<Product> mProductList;
    private Context mContext;

    Product mSingleProduct;

    public ProductAdapter(List<Product> aProductList, Context ctx) {

        super(ctx, R.layout.row_product, aProductList);
        mProductList = aProductList;
        mContext = ctx;

    }

    public int getCount() {
        return mProductList.size();
    }

    public Product getItem(int position) {
        return mProductList.get(position);
    }


    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;


        ProductHolder holder = new ProductHolder();

        // First let's verify the convertView is not null
        if (convertView == null) {
            // This a new view we inflate the new layout
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            v = inflater.inflate(R.layout.row_product, parent, false);
            // Now we can fill the layout with the right values

            TextView productName = (TextView) v.findViewById(R.id.productName);

            TextView productPrice = (TextView) v.findViewById(R.id.productPrice);

            TextView productBrand = (TextView) v.findViewById(R.id.productBrand);

            ImageView productImage = (ImageView) v.findViewById(R.id.productImage);

            holder.productNameTv = productName;

            holder.productPriceTv = productPrice;

            holder.productBrandTv = productBrand;

            holder.productImageIv = productImage;

            v.setTag(holder);


        }else{

            holder = (ProductHolder) v.getTag();

        }


        mSingleProduct = mProductList.get(position);

        holder.productNameTv.setText(mSingleProduct.getProductName());
        holder.productPriceTv.setText(mSingleProduct.getProductPrice());
        holder.productBrandTv.setText(mSingleProduct.getProductBrand());

        Picasso.with(mContext).load(mSingleProduct.getProductImageUrl())
                .placeholder(R.drawable.noimage)
                .error(R.drawable.noimage)
                .into(holder.productImageIv);

        return v;
    }

	/* *********************************
	 * We use the holder pattern
	 * It makes the view faster and avoid finding the component
	 * **********************************/

    private static class ProductHolder {

        public ImageView productImageIv;
        public TextView productNameTv;
        public TextView productPriceTv;
        public TextView productBrandTv;

    }


}
